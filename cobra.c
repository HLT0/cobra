//------------------------------------------------------------------------------
//
//  Animated Cobra MK-III from Elite Classic using only Xlib
//
//  Compile:
//  g++ -Wall -Os cobra.c -o cobra -lX11 -lXext -lm
//
//  2017 by f*c
//
//------------------------------------------------------------------------------
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/extensions/Xdbe.h>
#include <X11/keysymdef.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*------------------------------------------------------------------------------
TODO:
--------------------------------------------------------------------------------
V Hidden Line stuff (oh boy!)
V Ships geometry not correct yet
V remove unnecessary stuff from source (2d funcs etc)
V small ship flickering inside big one while key pressed! why?
V double buffering
V animation loop without pressing keys
V adjust parms: near, far, eye, d, w, h etc
V what is the rotation algo of the ship in the elite classic start screen?
V changing rotation axis resets all angles to orig vals. not nice
V it seems the ship is slowly wobbeling while rotating => center of mass!!
V add polygon table, remove edges table
V seperate model data operations from presentation data operations
  (MODEL, VERTICES_OUTPUT_BUFFER)
v make all responsive to changed window size

- add CLI args to control some aspects of the program (line width, fps, etc)
- make a screensaver out of it
- add zoom in/zoom out (nth) - when win resized(?!)
- make auto-rotation random-ish, current auto-rotation is ok so far (nth)

- atm viewport_transform() does only scale the model. no translation yet. if
  model is translated to center of screen then look_at has to be changed too. 
  Best: dynamically change look_at to center_of_mass of the model
V get rid of DX, DY, DZ stuff in vertices, use CenterOfMass instead
V get rid of DIST_FACTOR
V add func world_init() to setup the ship in the world initially
V use keys to interact:
    V SPACE : stop/start auto-rotate
    V Left  : yaw left by one step
    V Right : yaw right by one step
    V Up    : pitch up by one step
    V Down  : pitch down by one step
    V n     : roll left by one step
    V m     : roll right by one step
    V r     : reset all angles to 0, stop auto-rotate
- when using keys: rotate model about model axises, not world axises!
------------------------------------------------------------------------------*/
//#define DEBUG
#define PI 3.1415926535897932f

//------------------------------------------------------------------------------
// types
//------------------------------------------------------------------------------
typedef struct {double x, y;} v2_t;
typedef struct {double x, y, z;} v3_t;
typedef struct {double x, y, z, w;} v4_t;
typedef struct {double
    x00,    x01,    x02,    x03,
    x10,    x11,    x12,    x13,
    x20,    x21,    x22,    x23,
    x30,    x31,    x32,    x33;
} m4_t;


//------------------------------------------------------------------------------
// globals
//------------------------------------------------------------------------------
Display *DISPLAY;
Window  WINDOW;
GC      GCON;

Visual  *VISUAL = NULL;
XdbeBackBuffer DBUFFER;
bool    IS_DBUFFER_ENABLED = false;

ulong   BACKGROUND_COLOR = 0;
ulong   FOREGROUND_COLOR = 0;
int     LINE_WIDTH = 1;

int     WIN_WIDTH  = 640; // double the
int     WIN_HEIGHT = 400; // resolution of C=64

int     CENTER_X = WIN_WIDTH / 2;
int     CENTER_Y = WIN_HEIGHT / 2;

double  TOP    = 0.0;
double  LEFT   = 0.0;
double  BOTTOM = (double)WIN_HEIGHT;
double  RIGHT  = (double)WIN_WIDTH;;

double  NEAR = 20.0;
double  FAR  = 600.0;
double  FOV  = 90.0 * PI / 180.0;

double  DZ   = 300.0;

v4_t    EYE  = {0.0,  0.0,  0.0,  1.0}; // camera position
v4_t    UP   = {0.0,  1.0,  0.0,  0.0}; // camera up
v4_t    AT   = {0.0,  0.0,   DZ,  0.0}; // camera look at
v4_t    COM  = {0.0,  0.0,  0.0,  0.0}; // center of mass

// store angles of model
double THETA_X = 0.0;
double THETA_Y = 0.0;
double THETA_Z = 0.0;


//------------------------------------------------------------------------------
// vertices
//------------------------------------------------------------------------------
#define VERTICES_COUNT 29
static v4_t BASE_MODEL[VERTICES_COUNT] = {
    //   X     Y      Z    W
    // ship
    {-123.0, -2.0, -40.0, 1.0},  //  0 A
    {-123.0, -2.0, -15.0, 1.0},  //  1 B
    { -33.0, -2.0,  72.0, 1.0},  //  2 C
    {  33.0, -2.0,  72.0, 1.0},  //  3 D
    { 123.0, -2.0, -15.0, 1.0},  //  4 E
    { 123.0, -2.0, -40.0, 1.0},  //  5 F
    { 103.0, 10.0, -40.0, 1.0},  //  6 G
    {   0.0, 15.0, -40.0, 1.0},  //  7 H
    {-103.0, 10.0, -40.0, 1.0},  //  8 I
    {   0.0, 20.0,  30.0, 1.0},  //  9 J
    { -33.0,-15.0, -40.0, 1.0},  // 10 K
    {  33.0,-15.0, -40.0, 1.0},  // 11 L

    // left main engine
    {  -5.0, 10.0, -40.0, 1.0},  // 12 M
    {  -5.0,-10.0, -40.0, 1.0},  // 13 N
    { -34.0, -8.0, -40.0, 1.0},  // 14 O
    { -34.0,  8.0, -40.0, 1.0},  // 15 P

    // righ main engine
    {   5.0, 10.0, -40.0, 1.0},  // 16 Q
    {   5.0,-10.0, -40.0, 1.0},  // 17 R
    {  34.0, -8.0, -40.0, 1.0},  // 18 S
    {  34.0,  8.0, -40.0, 1.0},  // 19 T

    // left small engine
    { -97.0,  5.0, -40.0, 1.0},  // 20 U
    {-106.0,  3.0, -40.0, 1.0},  // 21 V
    { -97.0, -1.0, -40.0, 1.0},  // 22 W

    // right small engine
    {  97.0,  5.0, -40.0, 1.0},  // 23 X
    { 106.0,  3.0, -40.0, 1.0},  // 24 Y
    {  97.0, -1.0, -40.0, 1.0},  // 25 Z

    // gun
    {  -0.1, -2.0,  72.0, 1.0},  // 26 Z1
    {   0.0, -2.0,  92.0, 1.0},  // 27 Z2
    {   0.1, -2.0,  72.0, 1.0}   // 28 Z3
};
static v4_t MODEL[VERTICES_COUNT];
static v4_t VERTICES_OUTPUT_BUFFER[VERTICES_COUNT];


//------------------------------------------------------------------------------
// polygons
//------------------------------------------------------------------------------
// ...a polygon is a list of indexes pointing to the vertices buffers
#define MAX_VERTICES_PER_POLYGON 10
typedef struct polygon_t {
    int v[MAX_VERTICES_PER_POLYGON];
} polygon_t;

static polygon_t POLYGONS [] = {
    {{0,1,8,0}},            // ABI      0   top left small backward triangle
    {{1,2,8,1}},            // BCI      1   top left front - outer triangle
    {{8,2,9,8}},            // ICJ      2   top left front - inner triangle
    {{8,9,7,8}},            // IJH      3   top left main triangle
    {{9,2,3,9}},            // JCD      4   top front center triangle
    {{7,9,6,7}},            // HJG      5   top right main triangle
    {{6,9,3,6}},            // GJD      6   top right front - inner triangle
    {{6,3,4,6}},            // GDE      7   top right front - outer triangle
    {{6,4,5,6}},            // GEF      8   top right small backward triangle
    {{0,8,7,6,5,11,10,0}},  // AIHGFLK  9   ship's back plane
    {{10,2,1,10}},          // KCB      10  bottom left
    {{10,11,3,2,10}},       // KLDC     11  bottom center
    {{11,4,3,11}},          // LED      12  bottom right
    {{0,10,1,0}},           // ACB      13  bottom left backer
    {{4,11,5,4}},           // DFE      14  bottom right backer
    {{12,13,14,15,12}},     // MNOP     15  main engine left
    {{19,18,17,16,19}},     // TSRQ     16  main engine right
    {{22,21,20,22}},        // WVU      17  aux engine left
    {{23,24,25,23}},        // XYZ      18  aux engine right
//    {{26,27,28,26}},        // Z1Z2Z3  19  gun
    {{-1,0,0,0}}            // EOL
};


//------------------------------------------------------------------------------
// get a random number. whatever
double uniform()
{
    return (double)(rand() / (RAND_MAX + 1.0));
}

//------------------------------------------------------------------------------
// thats for debug
/*
static inline void show_vertices()
{
    for (int z = 0; z < VERTICES_COUNT; z++)
    {
        printf("%2d : (x=%9.2f, y=%9.2f, z=%9.2f, w=%9.2f)\n",
                z,
                MODEL[z].x,
                MODEL[z].y,
                MODEL[z].z,
                MODEL[z].w);
    }
    printf("\n");
}
*/

//------------------------------------------------------------------------------
// react if window size is changed
static inline void adjust_to_new_window_dimensions()
{
    XWindowAttributes attr;

    XGetWindowAttributes(DISPLAY, WINDOW, &attr);
    if (WIN_WIDTH == attr.width && WIN_HEIGHT == attr.height)
        return;

    WIN_WIDTH  = attr.width;
    WIN_HEIGHT = attr.height;

    CENTER_X = WIN_WIDTH / 2;
    CENTER_Y = WIN_HEIGHT / 2;

    RIGHT  = (double)WIN_WIDTH;
    BOTTOM = (double)WIN_HEIGHT;    
}

//------------------------------------------------------------------------------
// init the xlib stuff
static inline void x_init ()
{
    DISPLAY = XOpenDisplay(NULL);
    if (DISPLAY == NULL)
    {
        fprintf(stderr, "Cannot open display\n");
        exit(EXIT_FAILURE);
    }

    BACKGROUND_COLOR = BlackPixel (DISPLAY, DefaultScreen (DISPLAY));
    FOREGROUND_COLOR = WhitePixel (DISPLAY, DefaultScreen (DISPLAY));

    int major, minor;
    if (XdbeQueryExtension(DISPLAY, &major, &minor))
    {
        int numScreens = 1;
        Drawable screens[] = { DefaultRootWindow(DISPLAY) };
        XdbeScreenVisualInfo *info = XdbeGetVisualInfo(DISPLAY, screens, &numScreens);
        if (!info || numScreens < 1 || info->count < 1)
        {
            fprintf(stderr, "No visuals support Xdbe\n");
            exit(EXIT_FAILURE);
        }

        // Choosing the first one, seems that they have all perflevel of 0,
        // and the depth varies.
        XVisualInfo xvisinfo_templ;

        // We know there's at least one
        xvisinfo_templ.visualid = info->visinfo[0].visual; 

        // As far as I know, screens are densely packed, so we can assume that if
        // at least 1 exists, it's screen 0.
        xvisinfo_templ.screen = 0;
        xvisinfo_templ.depth = info->visinfo[0].depth;

        int matches;
        XVisualInfo *xvisinfo_match = XGetVisualInfo(DISPLAY,
                                                     VisualIDMask | VisualScreenMask | VisualDepthMask,
                                                     &xvisinfo_templ,
                                                     &matches
        );                            
        if (!xvisinfo_match || matches < 1)
        {
            fprintf(stderr, "Couldn't match a Visual with double buffering\n");
            exit(EXIT_FAILURE);
        }
        
        // We can use Visual from the match
        VISUAL = xvisinfo_match->visual;

#ifdef DEBUG
        printf("Double buffering enabled\n");
#endif
        IS_DBUFFER_ENABLED = true;
    }
    else
    {
#ifdef DEBUG
        printf("Double buffering not available\n");
#endif
        IS_DBUFFER_ENABLED = false;
    }

    if (IS_DBUFFER_ENABLED)
    {
        unsigned long xAttrMask = CWBackPixel;
        XSetWindowAttributes xAttr;
        xAttr.background_pixel = BACKGROUND_COLOR;
        WINDOW = XCreateWindow(DISPLAY,
                               DefaultRootWindow(DISPLAY),
                               0, 0,
                               WIN_WIDTH, WIN_HEIGHT,
                               0,
                               CopyFromParent,
                               CopyFromParent,
                               VISUAL,
                               xAttrMask,
                               &xAttr);
    }
    else
    {
        WINDOW = XCreateSimpleWindow(DISPLAY, DefaultRootWindow(DISPLAY),
                                     0, 0,
                                     WIN_WIDTH, WIN_HEIGHT,
                                     5,
                                     FOREGROUND_COLOR,
                                     BACKGROUND_COLOR);
    }

    if (!WINDOW)
    {
        fprintf(stderr, "Failed to create window\n");
        exit(EXIT_FAILURE);
    }

    if (IS_DBUFFER_ENABLED)
    {
        DBUFFER = XdbeAllocateBackBufferName(DISPLAY, WINDOW, XdbeBackground);
        GCON = XCreateGC (DISPLAY, DBUFFER, 0, 0);
    }
    else
    {
        GCON = XCreateGC (DISPLAY, WINDOW, 0, 0);
    }

    XSetStandardProperties (DISPLAY, WINDOW,
                            "Cobra Classic - 'q' to quit",
                            "Cobra Classic",
                            None,
                            NULL,
                            0,
                            NULL);

    // adjust to changed window attributes
    adjust_to_new_window_dimensions();

    // event types watched
    XSelectInput (DISPLAY, WINDOW, StructureNotifyMask | ExposureMask | KeyPressMask);
    XMapWindow(DISPLAY, WINDOW);

    // colors
    XSetForeground (DISPLAY, GCON, FOREGROUND_COLOR);
    XSetBackground (DISPLAY, GCON, BACKGROUND_COLOR);

    XFlush(DISPLAY);
}

//------------------------------------------------------------------------------
// do a clean shutdown of xlib
static inline void x_close ()
{
    XFreeGC (DISPLAY, GCON);
    XDestroyWindow (DISPLAY, WINDOW);
    XCloseDisplay (DISPLAY);
    exit(EXIT_SUCCESS);
}

//------------------------------------------------------------------------------
// draw a line between two points using XDrawLine()
static inline void x_line (int x1, int y1, int x2, int y2)
{
    // shift coords from top-left corner to center of window
    int c_x1 = CENTER_X + x1;
    int c_y1 = CENTER_Y - y1;
    int c_x2 = CENTER_X + x2;
    int c_y2 = CENTER_Y - y2;

    // draw according to dbuffer setting
    if(IS_DBUFFER_ENABLED)
        XDrawLine(DISPLAY, DBUFFER, GCON, c_x1, c_y1, c_x2, c_y2);
    else
        XDrawLine(DISPLAY, WINDOW,  GCON, c_x1, c_y1, c_x2, c_y2);
#ifdef DEBUG
    printf("(%d,%d) -> (%d,%d)\n", c_x1, c_y1, c_x2, c_y2);
#endif
}

//------------------------------------------------------------------------------
// subtract vector4 b from vector4 a
static inline v4_t sub_v4v4 (v4_t a, v4_t b)
{
    v4_t diff = {
        a.x  -  b.x,    // x
        a.y  -  b.y,    // y
        a.z  -  b.z,    // z
        a.w  -  b.w     // w
//        1.0             // w
    };
    return diff;
}

//------------------------------------------------------------------------------
// multiply vector4 a by a scalar (scalar product)
static inline v4_t scalar_mult_v4 (v4_t a, double scalar)
{
    v4_t sprod = {
        a.x * scalar,   // x
        a.y * scalar,   // y
        a.z * scalar,   // z
        a.w * scalar,   // w
    };
    return sprod;
}

//------------------------------------------------------------------------------
// multiply a matrix4 by a vector4
static inline v4_t mult_m4v4 (m4_t M, v4_t a)
{
    v4_t Mxa = {
        M.x00*a.x  +  M.x01*a.y  +  M.x02*a.z  +  M.x03*a.w,    // x
        M.x10*a.x  +  M.x11*a.y  +  M.x12*a.z  +  M.x13*a.w,    // y
        M.x20*a.x  +  M.x21*a.y  +  M.x22*a.z  +  M.x23*a.w,    // z
        M.x30*a.x  +  M.x31*a.y  +  M.x32*a.z  +  M.x33*a.w     // w
    };
    return Mxa;
}

//------------------------------------------------------------------------------
// multiply matrix4 A by matrix4 B
static inline m4_t mult_m4m4 (m4_t A, m4_t B)
{
    m4_t AxB = {
        A.x00*B.x00  +  A.x01*B.x10  +  A.x02*B.x20  +  A.x03*B.x30,    // x00
        A.x00*B.x01  +  A.x01*B.x11  +  A.x02*B.x21  +  A.x03*B.x31,    // x01
        A.x00*B.x02  +  A.x01*B.x12  +  A.x02*B.x22  +  A.x03*B.x32,    // x02
        A.x00*B.x03  +  A.x01*B.x13  +  A.x02*B.x23  +  A.x03*B.x33,    // x03

        A.x10*B.x00  +  A.x11*B.x10  +  A.x12*B.x20  +  A.x13*B.x30,    // x10
        A.x10*B.x01  +  A.x11*B.x11  +  A.x12*B.x21  +  A.x13*B.x31,    // x11
        A.x10*B.x02  +  A.x11*B.x12  +  A.x12*B.x22  +  A.x13*B.x32,    // x12
        A.x10*B.x03  +  A.x11*B.x13  +  A.x12*B.x23  +  A.x13*B.x33,    // x13

        A.x20*B.x00  +  A.x21*B.x10  +  A.x22*B.x20  +  A.x23*B.x30,    // x20
        A.x20*B.x01  +  A.x21*B.x11  +  A.x22*B.x21  +  A.x23*B.x31,    // x21
        A.x20*B.x02  +  A.x21*B.x12  +  A.x22*B.x22  +  A.x23*B.x32,    // x22
        A.x20*B.x03  +  A.x21*B.x13  +  A.x22*B.x23  +  A.x23*B.x33,    // x23

        A.x30*B.x00  +  A.x31*B.x10  +  A.x32*B.x20  +  A.x33*B.x30,    // x30
        A.x30*B.x01  +  A.x31*B.x11  +  A.x32*B.x21  +  A.x33*B.x31,    // x31
        A.x30*B.x02  +  A.x31*B.x12  +  A.x32*B.x22  +  A.x33*B.x32,    // x32
        A.x30*B.x03  +  A.x31*B.x13  +  A.x32*B.x23  +  A.x33*B.x33     // x33
    };
    return AxB;
}

//------------------------------------------------------------------------------
// calc the dot product of vector4 a and vector4 b
static inline double dot_v4v4 (v4_t a, v4_t b)
{
    return  a.x*b.x  +  a.y*b.y  +  a.z*b.z;
}

//------------------------------------------------------------------------------
// calc the cross product of vector4 a and vector4 b
static inline v4_t cross_v4v4 (v4_t a, v4_t b)
{
    v4_t axb = {
        a.y*b.z  -  a.z*b.y,    // x
        a.z*b.x  -  a.x*b.z,    // y
        a.x*b.y  -  a.y*b.x,    // z
        1.0                     // w
    };
    return axb;
}

//------------------------------------------------------------------------------
// calc the length of a vector4 a
static inline double length_v4 (v4_t a)
{
    return sqrt(a.x*a.x  +  a.y*a.y  +  a.z*a.z);
}

//------------------------------------------------------------------------------
// normalize a vector4 a
static inline v4_t norm_v4 (v4_t a)
{
    double len = length_v4(a);
    v4_t norm = {
        a.x  /  len,    // x
        a.y  /  len,    // y
        a.z  /  len,    // z
        1.0             // w
    };
    return norm;
}

//------------------------------------------------------------------------------
// calc the determinant of a vector4 a
static inline double det_v4 (m4_t M)
{
    return -1.0;
}

//------------------------------------------------------------------------------
// calc the inverse matrix of a matrix4 M
static inline m4_t inv_m4 (m4_t M)
{
    return M;
}

//------------------------------------------------------------------------------
// apply M to MODEL
static inline void apply_to_model (m4_t M)
{
    for (int i = 0; i < VERTICES_COUNT; i++)
    {
        MODEL[i] = mult_m4v4 (M, MODEL[i]);
//        MODEL[i] = mult_m4v4 (M, BASE_MODEL[i]);
    }
}

//------------------------------------------------------------------------------
// apply M to VERTICES_OUTPUT_BUFFER
static inline void apply_to_vertices_output_buffer (m4_t M)
{
    for (int i = 0; i < VERTICES_COUNT; i++)
    {
        VERTICES_OUTPUT_BUFFER[i] = mult_m4v4 (M, VERTICES_OUTPUT_BUFFER[i]);
    }
}

//------------------------------------------------------------------------------
// get identity matrix
static inline m4_t identity_matrix ()
{
    m4_t M = {
        1.0,    0.0,    0.0,    0.0,
        0.0,    1.0,    0.0,    0.0,
        0.0,    0.0,    1.0,    0.0,
        0.0,    0.0,    0.0,    1.0
    };
    return M;
}

//------------------------------------------------------------------------------
// get rotation matrix for angle theta about x axis
static inline m4_t rotation_matrix_x (double theta)
{
    double s = sin(theta * PI / 180.0);
    double c = cos(theta * PI / 180.0);
    m4_t M = {
        1.0,    0.0,    0.0,    0.0,
        0.0,    c,      s,      0.0,
        0.0,   -s,      c,      0.0,
        0.0,    0.0,    0.0,    1.0
    };
    return M;
}

//------------------------------------------------------------------------------
// get rotation matrix for angle theta about y axis
static inline m4_t rotation_matrix_y (double theta)
{
    double s = sin(theta * PI / 180.0);
    double c = cos(theta * PI / 180.0);
    m4_t M = {
        c,      0.0,   -s,      0.0,
        0.0,    1.0,    0.0,    0.0,
        s,      0.0,    c,      0.0,
        0.0,    0.0,    0.0,    1.0
    };
    return M;
}

//------------------------------------------------------------------------------
// get rotation matrix for angle theta about y axis
static inline m4_t rotation_matrix_z (double theta)
{
    double s = sin(theta * PI / 180.0);
    double c = cos(theta * PI / 180.0);
    m4_t M = {
        c,      s,      0.0,    0.0,
       -s,      c,      0.0,    0.0,
        0.0,    0.0,    1.0,    0.0,
        0.0,    0.0,    0.0,    1.0
    };
    return M;
}

//------------------------------------------------------------------------------
// get rotation matrix for angle theta about a vector4 v
static inline m4_t rotation_matrix_v4 (v4_t v, double theta)
{
    v = norm_v4(v);
    double s = sin(theta * PI / 180.0);
    double c = cos(theta * PI / 180.0);
    double o = 1.0 - c;
    m4_t M = {
        o*v.x*v.x+c,        o*v.x*v.y-v.z*s,    o*v.z*v.x+v.y*s,    0.0,
        o*v.x*v.y+v.z*s,    o*v.y*v.y+c,        o*v.y*v.z-v.x*s,    0.0,
        o*v.z*v.x-v.y*s,    o*v.y*v.z+v.x*s,    o*v.z*v.z+c,        0.0,
        0.0,                0.0,                0.0,                1.0
    };
    return M;
}

//------------------------------------------------------------------------------
// get translation matrix for dx, dy and dz
static inline m4_t translation_matrix (double dx, double dy, double dz)
{
    m4_t M = {
        1.0,    0.0,    0.0,    dx,
        0.0,    1.0,    0.0,    dy,
        0.0,    0.0,    1.0,    dz,
        0.0,    0.0,    0.0,    1.0
    };
    return M;
}

//------------------------------------------------------------------------------
// get scale matrix for factor fact along x axis
static inline m4_t scale_matrix_x (double fact)
{
    m4_t M = {
        fact,   0.0,    0.0,    0.0,
        0.0,    1.0,    0.0,    0.0,
        0.0,    0.0,    1.0,    0.0,
        0.0,    0.0,    0.0,    1.0
    };
    return M;
}

//------------------------------------------------------------------------------
// get scale matrix for factor fact along y axis
static inline m4_t scale_matrix_y (double fact)
{
    m4_t M = {
        1.0,    0.0,    0.0,    0.0,
        0.0,    fact,   0.0,    0.0,
        0.0,    0.0,    1.0,    0.0,
        0.0,    0.0,    0.0,    1.0
    };
    return M;
}

//------------------------------------------------------------------------------
// get scale matrix for factor fact along z axis
static inline m4_t scale_matrix_z (double fact)
{
    m4_t M = {
        1.0,    0.0,    0.0,    0.0,
        0.0,    1.0,    0.0,    0.0,
        0.0,    0.0,    fact,   0.0,
        0.0,    0.0,    0.0,    1.0
    };
    return M;
}

//------------------------------------------------------------------------------
// get scale matrix for factor fact along all axises
static inline m4_t scale_matrix_xyz (double fact)
{
    m4_t M = {
        fact,   0.0,    0.0,    0.0,
        0.0,    fact,   0.0,    0.0,
        0.0,    0.0,    fact,   0.0,
        0.0,    0.0,    0.0,    1.0
    };
    return M;
}

//------------------------------------------------------------------------------
// translate MODEL by dx, dy and dz
static inline void translate_model (double dx = 0.0, double dy = 0.0, double dz = 0.0)
{
    apply_to_model(translation_matrix(dx, dy, dz));
}

//------------------------------------------------------------------------------
// rotate MODEL about x by theta
static inline void rotate_model_x (double theta)
{
    THETA_X += theta;
    if (THETA_X > 360.0) THETA_X -= 360.0;
    if (THETA_X <   0.0) THETA_X =  360.0 - THETA_X;

    translate_model(0.0, 0.0, -DZ);
    apply_to_model(rotation_matrix_x(theta));
//    apply_to_model(rotation_matrix_x(THETA_X));
    translate_model(0.0, 0.0, DZ);
}

//------------------------------------------------------------------------------
// rotate MODEL about y by theta
static inline void rotate_model_y (double theta)
{
    THETA_Y += theta;
    if (THETA_Y > 360.0) THETA_Y -= 360.0;
    if (THETA_Y <   0.0) THETA_Y  = 360.0 - THETA_Y;

    translate_model(0.0, 0.0, -DZ);
    apply_to_model(rotation_matrix_y(theta));
//    apply_to_model(rotation_matrix_y(THETA_Y));
    translate_model(0.0, 0.0, DZ);
}

//------------------------------------------------------------------------------
// rotate MODEL about z by theta
static inline void rotate_model_z (double theta)
{
    THETA_Z += theta;
    if (THETA_Z > 360.0) THETA_Z -= 360.0;
    if (THETA_Z <   0.0) THETA_Z  = 360.0 - THETA_Z;

    translate_model(0.0, 0.0, -DZ);
    apply_to_model(rotation_matrix_z(theta));
//    apply_to_model(rotation_matrix_z(THETA_Z));
    translate_model(0.0, 0.0, DZ);
}

//------------------------------------------------------------------------------
// scale MODEL along x by fact
static inline void scale_model_x (double fact)
{
    translate_model(0.0, 0.0, -DZ);
    apply_to_model(scale_matrix_x(fact));
    translate_model(0.0, 0.0, DZ);
}

//------------------------------------------------------------------------------
// scale MODEL along y by fact
static inline void scale_model_y (double fact)
{
    translate_model(0.0, 0.0, -DZ);
    apply_to_model(scale_matrix_y(fact));
    translate_model(0.0, 0.0, DZ);
}

//------------------------------------------------------------------------------
// scale MODEL along z by fact
static inline void scale_model_z (double fact)
{
    translate_model(0.0, 0.0, -DZ);
    apply_to_model(scale_matrix_z(fact));
    translate_model(0.0, 0.0, DZ);
}

//------------------------------------------------------------------------------
// scale MODEL along all axises by fact
static inline void scale_model_xyz (double fact)
{
    translate_model(0.0, 0.0, -DZ);
    apply_to_model(scale_matrix_xyz(fact));
    translate_model(0.0, 0.0, DZ);
}

//------------------------------------------------------------------------------
// calc the center of mass of the model to get a rotation center
static inline v4_t calc_center_of_mass ()
{
    double sum_x = 0.0;
    double sum_y = 0.0;
    double sum_z = 0.0;

    for (int i = 0; i < VERTICES_COUNT; i++)
    {
        sum_x += MODEL[i].x;
        sum_y += MODEL[i].y;
        sum_z += MODEL[i].z;
    }
    COM.x = sum_x / VERTICES_COUNT;
    COM.y = sum_y / VERTICES_COUNT;
    COM.z = sum_z / VERTICES_COUNT;
    COM.w = 1.0;
    return COM;
}

//------------------------------------------------------------------------------
// apply camera transform to VERTICES_OUTPUT_BUFFER (!)
static inline void camera_transform ()
{
    v4_t za = norm_v4(sub_v4v4(EYE, AT));
    v4_t xa = norm_v4(cross_v4v4(UP, za));
    v4_t ya = cross_v4v4(za, xa);

    m4_t M = {
        xa.x,              ya.x,              za.x,             0.0,
        xa.y,              ya.y,              za.y,             0.0,
        xa.z,              ya.z,              za.z,             0.0,
       -dot_v4v4(xa,EYE), -dot_v4v4(ya,EYE), -dot_v4v4(za,EYE), 1.0
    };
    apply_to_vertices_output_buffer(M);
}

//------------------------------------------------------------------------------
// apply projection transform to VERTICES_OUTPUT_BUFFER (!)
static inline void projection_transform ()
{
    // apply projection matrix
    double W = 1.0 / tan(FOV * 0.5);
    double H = 1.0 / tan(FOV * 0.5);
    
    double A = FAR / (NEAR - FAR);
    double B = (NEAR * FAR) / (NEAR - FAR);

    m4_t M = {
        W,       0.0,    0.0,    0.0,
        0.0,     H,      0.0,    0.0,
        0.0,     0.0,    A,     -1.0,
        0.0,     0.0,    B,      0.0
    };
    apply_to_vertices_output_buffer(M);

    // perspective divide
    double w_len = 0.0;
    for (int i = 0; i < VERTICES_COUNT; i++)
    {
        w_len = 1.0 / VERTICES_OUTPUT_BUFFER[i].w;
        VERTICES_OUTPUT_BUFFER[i] = scalar_mult_v4(VERTICES_OUTPUT_BUFFER[i], w_len);
    }
}

//------------------------------------------------------------------------------
// apply viewport transform to VERTICES_OUTPUT_BUFFER (!)
static inline void viewport_transform ()
{
    m4_t M = {
       20.0,    0.0,    0.0,   0.0,
        0.0,   20.0,    0.0,   0.0,
        0.0,    0.0,    1.0,   0.0,
        0.0,    0.0,    0.0,   1.0
    };  
    apply_to_vertices_output_buffer(M);   
}

//------------------------------------------------------------------------------
// apply world transform to VERTICES_OUTPUT_BUFFER (!)
static inline void world_transform ()
{
    apply_to_vertices_output_buffer(identity_matrix());
}

//------------------------------------------------------------------------------
// check if a polygon is visible to the viewer (backface culling)
static inline bool is_polygon_visible (polygon_t polygon)
{
    // get first 3 vertices
    v4_t vertice_A = VERTICES_OUTPUT_BUFFER[polygon.v[0]];
    v4_t vertice_B = VERTICES_OUTPUT_BUFFER[polygon.v[1]];
    v4_t vertice_C = VERTICES_OUTPUT_BUFFER[polygon.v[2]];

    // Calc the vectors between the vertices AB and CD
    // (A - B) (not B - A) to get the right orientation for vector_AB
    v4_t vector_AB = sub_v4v4(vertice_A, vertice_B);

    // (B - C) cause it is already correctly orientated
    v4_t vector_BC = sub_v4v4(vertice_B, vertice_C);

    // Calc cross prod of both vectors to get the normal vector of the polygon
    v4_t cross =  cross_v4v4(vector_AB, vector_BC);

    // Calc dot product of normalized cross and eye - viewpoint
    v4_t view_line  = sub_v4v4(EYE, vertice_A);
    double dot = dot_v4v4(norm_v4(cross), norm_v4(view_line));

    // Case: dot > 0 => backfacing
    // Case: dot = 0 => viewed on edge
    // Case: dot < 0 => front facing
    return (dot < 0.0) ? true : false;
}

//------------------------------------------------------------------------------
// draw a polygon in 2d space
static inline void draw_2d_polygon(polygon_t polygon)
{
    // count vertices in polygon
    int vertice_count = 0;
    while (polygon.v[++vertice_count] != polygon.v[0]);

    // project polygon's vertices from 3d -> 2d
    double d = NEAR - EYE.z;
    v2_t vertices_2d[MAX_VERTICES_PER_POLYGON];
    v3_t vertice_3d;
    for (int i = 0; i <= vertice_count; i++)
    {
        vertice_3d.x = VERTICES_OUTPUT_BUFFER[polygon.v[i]].x;
        vertice_3d.y = VERTICES_OUTPUT_BUFFER[polygon.v[i]].y;
        vertice_3d.z = VERTICES_OUTPUT_BUFFER[polygon.v[i]].z;

        vertices_2d[i].x = (int) (d * (vertice_3d.x / vertice_3d.z));
        vertices_2d[i].y = (int) (d * (vertice_3d.y / vertice_3d.z));
    }

    // draw all polygon vertices in order in 2d
    for (int i = 0; i < vertice_count; i++)
    {
        x_line(vertices_2d[i  ].x, vertices_2d[i  ].y,
               vertices_2d[i+1].x, vertices_2d[i+1].y);
    }
}

//------------------------------------------------------------------------------
// draw all visible polygons in 2d space
static inline void draw_2d_polygons()
{
    int i = 0;
    while (POLYGONS[i].v[0] != -1)
    {
        if (is_polygon_visible(POLYGONS[i]))
            draw_2d_polygon(POLYGONS[i]);
        ++i;
    }
}

//------------------------------------------------------------------------------
// redraw the whole scene
static inline void draw_scene()
{
    if(!IS_DBUFFER_ENABLED)
        XClearWindow(DISPLAY, WINDOW);
/*
    XSetLineAttributes(DISPLAY, GCON, 1, LineOnOffDash, CapNotLast, JoinMiter);
    x_line(CENTER_X,TOP,CENTER_X,BOTTOM);
    x_line(0,CENTER_Y,RIGHT,CENTER_Y);

    int e=100;
    int step=25;
    int solid=100;

    // to right
    XSetLineAttributes(DISPLAY, GCON, 0, LineOnOffDash, CapNotLast, JoinMiter);
    for(int x = CENTER_X; x < RIGHT; x += step)
    {
        if (x % solid == 0)
            XSetLineAttributes(DISPLAY, GCON, 0, LineSolid, CapNotLast, JoinMiter);
        else
            XSetLineAttributes(DISPLAY, GCON, 0, LineOnOffDash, CapNotLast, JoinMiter);
        x_line(x,CENTER_Y-e,x,CENTER_Y+e);
    }
    // to left
    XSetLineAttributes(DISPLAY, GCON, 0, LineOnOffDash, CapNotLast, JoinMiter);
    for(int x = CENTER_X; x > LEFT; x -= step)
    {
        if (x % solid == 0)
            XSetLineAttributes(DISPLAY, GCON, 0, LineSolid, CapNotLast, JoinMiter);
        else
            XSetLineAttributes(DISPLAY, GCON, 0, LineOnOffDash, CapNotLast, JoinMiter);
        x_line(x,CENTER_Y-e,x,CENTER_Y+e);
    }
    // to down
    XSetLineAttributes(DISPLAY, GCON, 0, LineOnOffDash, CapNotLast, JoinMiter);
    for(int y = CENTER_Y; y < BOTTOM; y += step)
    {
        if (y % solid == 0)
            XSetLineAttributes(DISPLAY, GCON, 0, LineSolid, CapNotLast, JoinMiter);
        else
            XSetLineAttributes(DISPLAY, GCON, 0, LineOnOffDash, CapNotLast, JoinMiter);
        x_line(CENTER_X-e, y, CENTER_X+e, y);
    }
    // to up
    XSetLineAttributes(DISPLAY, GCON, 0, LineOnOffDash, CapNotLast, JoinMiter);
    for(int y = CENTER_Y; y > TOP; y -= step)
    {
        if (y % solid == 0)
            XSetLineAttributes(DISPLAY, GCON, 0, LineSolid, CapNotLast, JoinMiter);
        else
            XSetLineAttributes(DISPLAY, GCON, 0, LineOnOffDash, CapNotLast, JoinMiter);
        x_line(CENTER_X-e, y, CENTER_X+e, y);
    }
*/

    //copy model vertices to output buffer for transforms
    for (int i = 0; i < VERTICES_COUNT; i++)
    {
        VERTICES_OUTPUT_BUFFER[i] = MODEL[i];
        // ...from here on we work on VERTICES_OUTPUT_BUFFER
    }

    // do the 3d stuff
    world_transform();
    camera_transform();
    projection_transform();
    viewport_transform();

    // draw stuff in 2d
    XSetLineAttributes(DISPLAY, GCON, LINE_WIDTH, LineSolid, CapNotLast, JoinMiter);
    draw_2d_polygons();

    // Double buffering
    if(IS_DBUFFER_ENABLED)
    {
        XdbeSwapInfo swapInfo;
        swapInfo.swap_window = WINDOW;
        swapInfo.swap_action = XdbeBackground;
        XdbeSwapBuffers(DISPLAY, &swapInfo, 1);
    }

//#ifdef DEBUG
    printf ("X:%8.1f   Y:%8.1f   Z:%8.1f\n", THETA_X, THETA_Y, THETA_Z);
//#endif
    
}

//------------------------------------------------------------------------------
// change angles of ship with each call
static inline void auto_angle(double &tx, double &ty, double &tz)
{
    static double dx   = 0.0;
    const  double amp  = 1.1;
    const  double wlen = 0.015;

    tx = sin(dx * wlen) * amp + (amp * 0.75);
    ty = -cos(dx * wlen) * amp + (amp * 0.75);
    tz = -cos(dx * wlen) * amp + (amp * 0.75);

    if (dx > 100000000.0f)
        dx = 0.0;
    dx += 1.0;
}

//------------------------------------------------------------------------------
static inline void model_init()
{
    for (int i = 0; i < VERTICES_COUNT; i++)
        MODEL[i] = BASE_MODEL[i];

    // set inital position of ship
    v4_t com = calc_center_of_mass();
    translate_model(com.x, com.y, (DZ - com.z));
#ifdef DEBUG
    printf("center of mass: %3.6f, %3.6f, %3.6f \n", com.x, com.y, com.z);
#endif
}

//------------------------------------------------------------------------------
int main ()
{

    srand(time(NULL));
    x_init();
    model_init();

    double delta_theta_x = 0.0;
    double delta_theta_y = 0.0;
    double delta_theta_z = 0.0;
    double dt = 1.0; // stepping angle
    bool stepping = false; // stepping activated

    int x11_fd = ConnectionNumber( DISPLAY );
    fd_set readfds;
    timeval timeout;
    XEvent event;
    for(;;)
    {
        FD_ZERO(&readfds);
        FD_SET(x11_fd, &readfds);

        // consider timeout to be undefined after select()
        timeout.tv_sec = 0;
        timeout.tv_usec = 32768; // (1 / tv_usec) ~ fps

        // select() returns 0 if an event was received
        // or !0 if the timeout fired. (thnx 2 AngryLlama [1])
        select((x11_fd + 1), &readfds, 0, 0, &timeout);

        // we can now check for pending events before handling them
        while (XPending(DISPLAY))
        {
            XNextEvent(DISPLAY, &event);

            // handle expose
            if (event.type == Expose && event.xexpose.count == 0)
            {
                adjust_to_new_window_dimensions();
            }       
            // handle 'q'
            else if (event.type == KeyPress &&
                     XLookupKeysym(&event.xkey, 0) == XK_q )
            {
                x_close();
            }
            // handle 'Right' - rotate 1 dt step about y axis (nose right)
            else if (event.type == KeyPress &&
                     XLookupKeysym(&event.xkey, 0) == XK_Right)
            {
                delta_theta_y = dt;
                rotate_model_y(delta_theta_y);
                stepping = true;
            }
            // handle 'Left' - rotate 1 dt step about y axis (nose left)
            else if (event.type == KeyPress &&
                     XLookupKeysym(&event.xkey, 0) == XK_Left)
            {
                delta_theta_y =  -dt;                
                rotate_model_y(delta_theta_y);
                stepping = true;
            }
            // handle 'Up' - rotate 1 dt step about x axis (nose-up)
            else if (event.type == KeyPress &&
                     XLookupKeysym(&event.xkey, 0) == XK_Up)
            {
                delta_theta_x = dt;
                rotate_model_x(delta_theta_x);
                stepping = true;
            }
            // handle 'Down' - rotate 1 dt step about x axis (nose down)
            else if (event.type == KeyPress &&
                     XLookupKeysym(&event.xkey, 0) == XK_Down)
            {
                delta_theta_x = -dt;
                rotate_model_x(delta_theta_x);
                stepping = true;
            }
            // handle 'n' - rotate 1 dt step about z axis (roll left)
            else if (event.type == KeyPress &&
                     XLookupKeysym(&event.xkey, 0) == XK_n)
            {
                delta_theta_z = dt;
                rotate_model_z(delta_theta_z);
                stepping = true;
            }
            // handle 'm' - rotate 1 dt step about z axis (roll right)
            else if (event.type == KeyPress &&
                     XLookupKeysym(&event.xkey, 0) == XK_m)
            {
                delta_theta_z = -dt;
                rotate_model_z(delta_theta_z);
                stepping = true;
            }
            // handle 'r' - reset all angles of model
            else if (event.type == KeyPress &&
                     XLookupKeysym(&event.xkey, 0) == XK_r)
            {
                model_init();
                delta_theta_x = 0.0;
                delta_theta_y = 0.0;
                delta_theta_z = 0.0;
                THETA_X = 0.0;
                THETA_Y = 0.0;
                THETA_Z = 0.0;
                stepping = true;
            }
            // handle 'SPACE' - pause/resume auto-rotate
            else if (event.type == KeyPress &&
                     XLookupKeysym(&event.xkey, 0) == XK_space)
            {
                stepping = !stepping;
            }
            else;
        }
        // auto-rotate
        if (!stepping)
        {
            // determine new auto-rotate angles and rotate model
            auto_angle(delta_theta_x, delta_theta_y, delta_theta_z);
            rotate_model_x(delta_theta_x);
            rotate_model_y(-delta_theta_y);
            rotate_model_z(delta_theta_z);
        }
        draw_scene();
    }
    return 0;
}



// Refs:
// [1]  http://www.linuxquestions.org/questions/programming-9/xnextevent-select-409355/#post2431345
